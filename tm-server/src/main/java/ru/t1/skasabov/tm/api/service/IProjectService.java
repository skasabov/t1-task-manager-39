package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.enumerated.Sort;
import ru.t1.skasabov.tm.enumerated.Status;
import ru.t1.skasabov.tm.model.Project;

import java.util.Collection;
import java.util.Date;
import java.util.List;

public interface IProjectService {

    @NotNull
    Project add(@Nullable Project model);

    @NotNull
    List<Project> findAll();

    @NotNull
    List<Project> findAll(@Nullable Sort sortType);

    @NotNull
    Collection<Project> addAll(@Nullable Collection<Project> models);

    @NotNull
    Collection<Project> set(@Nullable Collection<Project> models);

    @NotNull
    Boolean existsById(@Nullable String id);

    @Nullable
    Project findOneById(@Nullable String id);

    @Nullable
    Project findOneByIndex(@Nullable Integer index);

    int getSize();

    @NotNull
    Project removeOne(@Nullable Project model);

    @Nullable
    Project removeOneById(@Nullable String id);

    @Nullable
    Project removeOneByIndex(@Nullable Integer index);

    void removeAll(@Nullable Collection<Project> collection);

    void removeAll();

    @NotNull
    Boolean existsById(@Nullable String userId, @Nullable String id);

    @NotNull
    List<Project> findAll(@Nullable String userId);

    @NotNull
    List<Project> findAll(@Nullable String userId, @Nullable Sort sortType);

    @Nullable
    Project findOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project findOneByIndex(@Nullable String userId, @Nullable Integer index);

    int getSize(@Nullable String userId);

    @Nullable
    Project removeOneById(@Nullable String userId, @Nullable String id);

    @Nullable
    Project removeOneByIndex(@Nullable String userId, @Nullable Integer index);

    void removeAll(@Nullable String userId);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name);

    @NotNull
    Project create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Project create(
            @Nullable String userId,
            @Nullable String name, @Nullable String description,
            @Nullable Date dateBegin, @Nullable Date dateEnd
    );

    @NotNull
    Project updateById(
            @Nullable String userId,
            @Nullable String id, @Nullable String name, @Nullable String description
    );

    @NotNull
    Project updateByIndex(
            @Nullable String userId,
            @Nullable Integer index, @Nullable String name, @Nullable String description
    );

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

}
