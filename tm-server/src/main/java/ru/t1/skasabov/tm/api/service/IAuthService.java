package ru.t1.skasabov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.Session;
import ru.t1.skasabov.tm.model.User;

public interface IAuthService {

    @NotNull
    User registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    String login(@Nullable String login, @Nullable String password);

    void invalidate(@Nullable Session session);

    @NotNull
    Session validateToken(@Nullable String token);

}
