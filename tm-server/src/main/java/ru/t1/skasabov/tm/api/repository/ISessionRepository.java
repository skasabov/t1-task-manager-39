package ru.t1.skasabov.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.skasabov.tm.model.Session;

import java.util.List;

public interface ISessionRepository {

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    Boolean existsByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @NotNull
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    List<Session> findAllForUser(@NotNull @Param("userId") String userId);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    Session findOneByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    Session findOneByIndexForUser(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_session WHERE user_id = #{userId}")
    int getSizeForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void removeOneByIdForUser(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_session WHERE id IN (SELECT id FROM tm_session " +
            "WHERE user_id = #{userId} LIMIT 1 OFFSET #{index})")
    void removeOneByIndexForUser(@NotNull @Param("userId") String userId, @NotNull @Param("index") Integer index);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void removeAllForUser(@NotNull @Param("userId") String userId);

    @Insert("INSERT INTO tm_session (id, created, role, user_id) VALUES (#{id}, #{date}, " +
            "#{role}::role_type, #{userId})")
    void add(@NotNull Session model);

    @NotNull
    @Select("SELECT * FROM tm_session")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    List<Session> findAll();

    @NotNull
    @Select("SELECT COUNT(*) > 0 FROM tm_session WHERE id = #{id}")
    Boolean existsById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_session WHERE id = #{id}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    Session findOneById(@NotNull @Param("id") String id);

    @Nullable
    @Select("SELECT * FROM tm_session LIMIT 1 OFFSET #{index}")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "date", column = "created")
    })
    Session findOneByIndex(@NotNull @Param("index") Integer index);

    @Select("SELECT COUNT(*) FROM tm_session")
    int getSize();

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void removeOne(@NotNull Session model);

    @Delete("DELETE FROM tm_session WHERE id = #{id}")
    void removeOneById(@NotNull @Param("id") String id);

    @Delete("DELETE FROM tm_session WHERE id IN (SELECT id FROM tm_session LIMIT 1 OFFSET #{index})")
    void removeOneByIndex(@NotNull Integer index);

    @Delete("DELETE FROM tm_session")
    void removeAll();

}
