package ru.t1.skasabov.tm.taskmanager.repository;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import ru.t1.skasabov.tm.api.repository.IUserRepository;
import ru.t1.skasabov.tm.model.User;
import ru.t1.skasabov.tm.taskmanager.AbstractTest;
import ru.t1.skasabov.tm.util.HashUtil;

import java.util.List;

public class UserRepositoryTest extends AbstractTest {

    @NotNull
    private User cat;

    @NotNull
    private User mouse;

    @NotNull
    private IUserRepository userRepository;

    @NotNull
    private SqlSession sqlSession;

    @Before
    @SneakyThrows
    public void initRepository() {
        sqlSession = connectionService.getSqlSession();
        userRepository = sqlSession.getMapper(IUserRepository.class);
        userRepository.removeAll();
        cat = new User();
        cat.setLogin("cat");
        cat.setPasswordHash(HashUtil.salt(propertyService, "cat"));
        cat.setEmail("cat@cat");
        mouse = new User();
        mouse.setLogin("mouse");
        mouse.setPasswordHash(HashUtil.salt(propertyService, "mouse"));
        mouse.setEmail("mouse@mouse");
        userRepository.add(cat);
        userRepository.add(mouse);
    }

    @Test
    public void testUpdate() {
        mouse.setLastName("mouse");
        mouse.setFirstName("mouse");
        mouse.setMiddleName("mouse");
        userRepository.update(mouse);
        @Nullable final User actualUser = userRepository.findByLogin("mouse");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals("mouse", actualUser.getLastName());
        Assert.assertEquals("mouse", actualUser.getFirstName());
        Assert.assertEquals("mouse", actualUser.getMiddleName());
    }

    @Test
    public void testAdd() {
        final int expectedUsers = userRepository.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("dog");
        user.setPasswordHash(HashUtil.salt(propertyService, "dog"));
        userRepository.add(user);
        Assert.assertEquals(expectedUsers, userRepository.getSize());
    }

    @Test
    public void testClearAll() {
        userRepository.removeAll();
        Assert.assertEquals(0, userRepository.getSize());
    }

    @Test
    public void testFindAll() {
        @NotNull final List<User> userList = userRepository.findAll();
        Assert.assertEquals(userList.size(), userRepository.getSize());
    }

    @Test
    public void testFindById() {
        @Nullable final User actualUser = userRepository.findOneById(cat.getId());
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByIdUserNotFound() {
        Assert.assertNull(userRepository.findOneById("some_id"));
    }

    @Test
    public void testFindByLogin() {
        @Nullable final User actualUser = userRepository.findByLogin("cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByLoginUserNotFound() {
        Assert.assertNull(userRepository.findByLogin("dog"));
    }

    @Test
    public void testFindByEmail() {
        @Nullable final User actualUser = userRepository.findByEmail("cat@cat");
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(cat.getLogin(), actualUser.getLogin());
        Assert.assertEquals(cat.getEmail(), actualUser.getEmail());
        Assert.assertEquals(cat.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(cat.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByEmailUserNotFound() {
        Assert.assertNull(userRepository.findByEmail("dog@dog"));
    }

    @Test
    public void testFindByIndex() {
        @NotNull final User user = userRepository.findAll().get(0);
        @Nullable final User actualUser = userRepository.findOneByIndex(0);
        Assert.assertNotNull(actualUser);
        Assert.assertEquals(user.getLogin(), actualUser.getLogin());
        Assert.assertEquals(user.getEmail(), actualUser.getEmail());
        Assert.assertEquals(user.getPasswordHash(), actualUser.getPasswordHash());
        Assert.assertEquals(user.getRole(), actualUser.getRole());
    }

    @Test
    public void testFindByIndexUserNotFound() {
        userRepository.removeAll();
        Assert.assertNull(userRepository.findOneByIndex(0));
    }

    @Test
    public void testGetSize() {
        final int expectedNumberOfEntries = userRepository.getSize() + 1;
        @NotNull final User user = new User();
        user.setLogin("dog");
        user.setPasswordHash(HashUtil.salt(propertyService, "dog"));
        user.setEmail("dog@dog");
        userRepository.add(user);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testIsNotFoundById() {
        @NotNull final String validId = userRepository.findAll().get(0).getId();
        @NotNull final String invalidId = "some_id";
        Assert.assertFalse(userRepository.existsById(invalidId));
        Assert.assertTrue(userRepository.existsById(validId));
    }

    @Test
    public void testRemove() {
        final int expectedNumberOfEntries = userRepository.getSize() - 1;
        userRepository.removeOne(mouse);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveById() {
        final int expectedNumberOfEntries = userRepository.getSize() - 1;
        userRepository.removeOneById(mouse.getId());
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveByIndex() {
        final int expectedNumberOfEntries = userRepository.getSize() - 1;
        userRepository.removeOneByIndex(0);
        Assert.assertEquals(expectedNumberOfEntries, userRepository.getSize());
    }

    @Test
    public void testRemoveByIndexUserNotFound() {
        userRepository.removeAll();
        userRepository.removeOneByIndex(0);
        Assert.assertEquals(0, userRepository.getSize());
    }

    @Test
    public void testLoginExist() {
        Assert.assertTrue(userRepository.isLoginExist("cat"));
    }

    @Test
    public void testEmptyLoginExist() {
        Assert.assertFalse(userRepository.isLoginExist(""));
    }

    @Test
    public void testEmailExist() {
        Assert.assertTrue(userRepository.isEmailExist("cat@cat"));
    }

    @Test
    public void testEmptyEmailExist() {
        Assert.assertFalse(userRepository.isEmailExist(""));
    }

    @After
    @SneakyThrows
    public void clearRepository() {
        sqlSession.rollback();
        sqlSession.close();
    }

}
